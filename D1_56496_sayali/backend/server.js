const express = require('express')
const utils=require('./utils')
const cors = require('cors')

const app = express()
app.use(cors())
app.use(express.json())

app.get('/getmovie/:movie_title', (request, response) => {
  const { movie_title} = request.params

  const connection = utils.openConnection()

  const statement = `
  select * from Movie where movie_title = '${movie_title}' `
  connection.query(statement, (error, result) => {
    connection.end()
     if (result.length > 0) {
     
      response.send(utils.createResult(error, result));
    } else {
      response.send("movie not found !");
    }
   
  })
})

app.post('/addmovie', (request, response) => {
  const { movie_title, movie_release_date, movie_time, director_name} = request.body

  const connection = utils.openConnection()

  const statement = `
  insert into Movie values(default,'${movie_title}','${movie_release_date}','${movie_time}','${director_name}') `
  connection.query(statement, (error, result) => {
    connection.end()
     if (result.length > 0) {
     
      response.send(utils.createResult(error, result));
    } else {
      response.send("movie not inserted into table !");
    }
   
  })
})

app.put('/updatemovie', (request, response) => {
  const { movie_id,movie_release_date, movie_time} = request.body

  const connection = utils.openConnection()

  const statement = `
  update Movie set movie_release_date = '${movie_release_date}' and movie_time = '${movie_time}' where movie_id = ${movie_id}`
  connection.query(statement, (error, result) => {
    connection.end()
     if (result.length > 0) {
     
      response.send(utils.createResult(error, result));
    } else {
      response.send("movie not updated !");
    }
   
  })
})

app.delete('/deletemovie/:movie_title', (request, response) => {
  const { movie_title} = request.params

  const connection = utils.openConnection()

  const statement = `
  delete from Movie where movie_title = '${movie_title}' `
  connection.query(statement, (error, result) => {
    connection.end()
     if (result.length > 0) {
     
      response.send(utils.createResult(error, result));
    } else {
      response.send("movie not deleted !");
    }
   
  })
})

app.listen(4000, () => {
  console.log(`server started on port 4000`)
})